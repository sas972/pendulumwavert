package org.sas.pendulum;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App extends Frame implements Runnable
{
	World world = new World();
	List<Point2D.Double> lastPainted = new ArrayList<Point2D.Double>();
	private List<Double> positions = new ArrayList<Point2D.Double>();
	private double zoom;
	
	public void run() {
		world.printoutPendulums();
		long startTime = System.currentTimeMillis();
		positions = world.getPositions(0);
		zoom = calculateZoom(positions);
		for(;;){
			try {
				Thread.sleep(20);
				synchronized(positions){
					positions = world.getPositions((double)(System.currentTimeMillis() - startTime)/1000d);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			repaint();
		}
	}
	
	public App(){
		super("Pendulum Vawe");
		setSize(600,800);
	      addWindowListener(new WindowAdapter(){
	    	 @Override
	         public void windowClosing(WindowEvent windowEvent){
	            System.exit(0);
	         }        
	      }); 

	}
	
	@Override
	public void paint(Graphics g) {
	      Graphics2D g2 = (Graphics2D)g;
	      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		  RenderingHints.VALUE_ANTIALIAS_ON);
		  g2.setColor(Color.WHITE);
		  synchronized (this) {
			
			  for (Point2D.Double pos: lastPainted){
				  g2.fillOval(300+(int)(pos.x/zoom)-11, (int)(pos.y/zoom)-8, 22, 16);
			  }
			  lastPainted.clear();
			  g2.setColor(Color.BLACK);
			  if(null != positions)
				  synchronized(positions){
					  for(Point2D.Double pos: positions){
						  g2.fillOval(300+(int)(pos.x/zoom)-5, (int)(pos.y/zoom)-5, 10, 10);
						  lastPainted.add(pos);
					  }
				  }
		  }
	   } 
	
    public static void main( String[] args )
    {
        App app =new App();
        app.setVisible(true);
        new Thread(app).start();
    }
    
    private double calculateZoom(List<Double> positions){
    	Point2D.Double max = positions.get(getMaxPos(positions));
    	double lMax = Math.sqrt(max.x*max.x + max.y*max.y);
    	return lMax / (double)getSize().height * 1.2;
   }

	private int getMaxPos(List<Double> positions) {
		int maxPos = 0;
    	double yMax = 0;
    	for (int i = 0; i < positions.size(); ++i){
    		if (yMax < positions.get(i).y) {
    			yMax = positions.get(i).y;
    			maxPos = i;
    		}
    	}
		return maxPos;
	}
    @Override
    public void update(Graphics g){
    	paint(g);
    }

}
