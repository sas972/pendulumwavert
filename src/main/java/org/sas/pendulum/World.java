package org.sas.pendulum;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

public class World {

	List<Pendulum> pendulums = new ArrayList<Pendulum>();
	
	public World(){
		//http://www.arborsci.com/cool/pendulum-wave-seems-like-magic-but-its-physics 
		for ( double i = 0; i < 16;++i){
			pendulums.add(new Pendulum(2*Math.pow(18.0/(18.0+i), 2.0),10,Math.PI/25));
		}
	}
	
	public List<Point2D.Double> getPositions(double second) {
		List<Point2D.Double> posList = new ArrayList<Point2D.Double>();
		for (Pendulum pendulum : pendulums) {
			posList.add(pendulum.next(second));
		};
		return posList;
	}
	
	public void printoutPendulums() {
		for (Pendulum pendulum : pendulums) {
			System.out.println(pendulum.toString());
			System.out.println();
		};
	}

}
