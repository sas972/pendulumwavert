package org.sas.pendulum;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

import java.awt.geom.Point2D;

// http://hu.wikipedia.org/wiki/Matematikai_inga
public class Pendulum {
	private Point2D.Double pos = new Point2D.Double();
	double l, g, alpha, omega, beta, alpha0;
	
	public Pendulum(double l, double g, double alpha0) {
		super();
		this.l = l;
		this.g = g;
		this.alpha0 = alpha0;
		omega = Math.sqrt(g/this.l);
	}
	
	@Override
	public String toString(){
		return "l: " + l + " m\n"+ 
		"Omega: " + omega + " 1/s\n"+
		"T: " + ( Math.PI * 2 / omega ) + " s";
	}
	
	public  Point2D.Double next(double t){
		alpha = alpha0*cos(omega*t);
		pos.x = l*sin(alpha);
		pos.y = l*cos(alpha);
		return pos; 	
	}
	
}
